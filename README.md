# rtthread-esp-idf
RT-Thread针对ESP32的移植（使用Espressif Systems的IDF框架）

💡 这是一个临时性，用于开发的仓库，完成后这个仓库会被删除。整体规划是：先以早期移植过的RT-Thread v2.1.0版本为参考进行移植，把其中的idf更新到目前相对新的v4.2版本上，保证系统稳定运行，文件系统/网络协议，WiFI正常运行；然后再把RT-Thread更新到最新的git/master版本（v4.0.4左右）；运行稳定后，再把idf版本更新到最新的正式版，从而完成整体移植。

🚑 当前状态：RT-Thread v2.1.0 + idf v4.2能够简单运行系统和shell；正在处理WiFi对接中。

目前这份移植主要是针对idf-v4.2版本，需要把对应的版本下载到这个仓库下，同时包括对应的工具链

* https://dl.espressif.com/github_assets/espressif/esp-idf/releases/download/v4.2/esp-idf-v4.2.zip
* https://dl.espressif.com/dl/xtensa-esp32-elf-gcc8_4_0-esp-2020r3-win32.zip
* https://dl.espressif.com/dl/xtensa-esp32-elf-gcc8_4_0-esp-2020r3-linux-i686.tar.gz

github上的链接

* https://github.com/espressif/esp-idf/releases/download/v4.2/esp-idf-v4.2.zip
* https://github.com/espressif/crosstool-NG/releases/download/esp-2020r3/xtensa-esp32-elf-gcc8_4_0-esp-2020r3-win32.zip
* https://github.com/espressif/crosstool-NG/releases/download/esp-2020r3/xtensa-esp32-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz

把`esp-idf-v4.2.zip` 解压到`esp-idf-port`目录下，并改名成`esp-idf`；在展开了v4.2的esp-idf版本后，还需要打上esp-idf-port目录下的*.patch补丁，可以使用类似`git am 0001...patch`的方式来打补丁。

把`xtensa-esp32-elf-gcc8_4_0-esp-2020r3-win32.zip` 或 `xtensa-esp32-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz` 解压到`esp-idf-port`目录下，文件夹名称应该是`xtensa-esp32-elf`

有几个libhal.a中的.o并不能直接链接，需要从libhal.a中解出来，放到`esp-idf-port`下，假设当前是rtthread-esp-idf目录，使用下属指令来操作以获得这几个.o文件：

```txt
mkdir lib
cd lib
xtensa-esp32-elf-ar.exe x ../esp-idf-port/esp-idf/components/xtensa/esp32/libhal.a
cp lib\windowspill_asm.o esp-idf-port\windowspill_asm.o
cp lib\state_asm--save_extra_nw.o esp-idf-port\save_extra_nw.o
cp lib\state_asm--restore_extra_nw.o esp-idf-port\restore_extra_nw.o
```

💡 需要后续进行修正

## 编译

可以在env下，进入到esp-idf-port目录中，运行env.bat来设置环境变量。

然后退回到`rtthread-esp-idf`（这个仓库的根目录），然后运行`scons`

```bash
LINK rtthread-esp32.elf
xtensa-esp32-elf-size rtthread-esp32.elf
   text    data     bss     dec     hex filename
 126730   34732    2472  163934   2805e rtthread-esp32.elf
python esp-idf-port/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 elf2image --flash_mode "dio" --flash_freq "40m" --flash_size "4MB" -o rtthread.bin rtthread-esp32.elf
esptool.py v3.0
scons: done building targets.
```

最后会提示这个信息。
